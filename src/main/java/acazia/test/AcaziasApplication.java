package acazia.test;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AcaziasApplication {

	public static void main(String[] args) {
		SpringApplication.run(AcaziasApplication.class, args);
	}

}
