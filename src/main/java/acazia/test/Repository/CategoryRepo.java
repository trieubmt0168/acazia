package acazia.test.Repository;

import acazia.test.entity.Category;
import acazia.test.entity.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.Optional;

@Repository
public interface CategoryRepo extends JpaRepository<Category, String> {

    @Query(value = "select * from category where tag =:tag", nativeQuery = true)
    Optional<Category> findByTag(@Param("tag") String tag);

    @Query(value = "select * from category where tag=:tag", nativeQuery = true)
    Category findByTagAndTag(@Param("tag") String tag);

    @Query(value = "select * from product where category_tag=:categoryTag", nativeQuery = true)
    Product findByName(@Param("categoryTag") String categoryTag);

}
