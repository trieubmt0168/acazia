package acazia.test.Repository;

import acazia.test.entity.Category;
import acazia.test.entity.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository(value = "productRepo")
public interface ProductRepo extends JpaRepository<Product, Long> {
    //select p.name, p.category_tag,p.price,c.name from product p left join category c on p.category_tag = c.tag where c.name COLLATE UTF8_GENERAL_CI  like '%lap%' order by price desc;
    @Query(value = "select p.name as pName, p.category_tag,p.price, c.name as cName from product p left join category c on p.category_tag = c.tag " +
            " where c.name COLLATE UTF8_GENERAL_CI  like '%' :categoryName '%' order by price desc;", nativeQuery = true)
    List<Object[]> findByName(@Param("categoryName") String categoryName);
}
