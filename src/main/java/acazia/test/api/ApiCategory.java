package acazia.test.api;

import acazia.test.entity.Category;
import acazia.test.model.CategoryModel;
import acazia.test.service.ICategoryService;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/category")
public class ApiCategory {
    @Autowired
    ICategoryService categoryService;

    @GetMapping(value = "/list")
    @ResponseStatus(value = HttpStatus.OK)
    public List<Category> categoryList() {
        return categoryService.lsCategory();
    }

    @PostMapping(value = "/create")
    @ResponseStatus(value = HttpStatus.OK)
    public void addCategory(@RequestBody CategoryModel category) {
        categoryService.insert(category);
    }


    @GetMapping(value = "/{tag}")
    @ResponseStatus(value = HttpStatus.OK)
    public Optional<Category> findcate(@PathVariable String tag) {
        return categoryService.findByTag(tag);
    }

    @PutMapping(value = "/Update")
    @ResponseStatus(value = HttpStatus.OK)
    public void updateCategory(@RequestBody CategoryModel category) {
        categoryService.update(category);
    }

    @PostMapping(value = "/delete/{tag}")
    @ResponseStatus(value = HttpStatus.OK)
    public void deleteReservation(@PathVariable String tag) throws IOException {
        categoryService.delete(tag);

    }

}
