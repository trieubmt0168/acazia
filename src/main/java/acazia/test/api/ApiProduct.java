package acazia.test.api;

import acazia.test.entity.Product;
import acazia.test.model.ProductModel;
import acazia.test.service.IProductSevice;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/product")
public class ApiProduct {

    @Autowired
    IProductSevice productSevice;

    @GetMapping(value = "/list")
    @ResponseStatus(value = HttpStatus.OK)
    public List<Product> categoryList() {
        return productSevice.lsCategory();
    }

    @PostMapping(value = "/create")
    @ResponseStatus(value = HttpStatus.OK)
    public void addCategory(@RequestBody ProductModel product) {
        productSevice.insert(product);
    }
    @GetMapping(value = "/{id}")
    @ResponseStatus(value = HttpStatus.OK)
    public Optional<Product> findcate(@PathVariable Long id) {
        return productSevice.findById(id);
    }


    @PutMapping(value = "/Update")
    @ResponseStatus(value = HttpStatus.OK)
    public void updateProduct(@RequestBody ProductModel product) {
        productSevice.update(product);
    }

    @PostMapping(value = "/delete/{id}")
    @ResponseStatus(value = HttpStatus.OK)
    public void deleteReservation(@PathVariable Long id) {
        productSevice.delete(id);

    }
    @GetMapping(value = "/findByTag")
    @ResponseStatus(value = HttpStatus.OK)
    public List<Object[]> findbytag(@RequestParam("categoryName") String categoryName) {
        List<Object[]> lsResponse = productSevice.findByTag(categoryName);
        return lsResponse;
    }
}
