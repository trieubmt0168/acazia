package acazia.test.model;

import acazia.test.entity.Category;
import acazia.test.entity.Product;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class ProductModel {
    private Long Id;
    private String name;
    private Double price;
    private Category categoryTag;
    public Long getId() {
        return Id;
    }

    public void setId(Long id) {
        Id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Category getCategoryTag() {
        return categoryTag;
    }

    public void setCategoryTag(Category categoryTag) {
        this.categoryTag = categoryTag;
    }
}
