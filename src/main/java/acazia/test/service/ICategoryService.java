package acazia.test.service;

import acazia.test.entity.Category;
import acazia.test.entity.Product;
import acazia.test.model.CategoryModel;

import java.io.IOException;
import java.util.List;
import java.util.Optional;

public interface ICategoryService {
    List<Category> lsCategory();

    Object insert(CategoryModel category);

    Category update(CategoryModel category);

    void delete(String tag) throws IOException;

    Optional<Category> findByTag(String tag);

}
