package acazia.test.service;

import acazia.test.entity.Product;
import acazia.test.model.ProductModel;

import java.util.List;
import java.util.Optional;

public interface IProductSevice {
    List<Product> lsCategory();

    Object insert(ProductModel product);

    Product update(ProductModel product);

    void delete(Long id);

    Optional<Product> findById(Long id);

    List<Object[]> findByTag(String categoryName);
}
