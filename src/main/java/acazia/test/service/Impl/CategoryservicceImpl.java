package acazia.test.service.Impl;

import acazia.test.Repository.CategoryRepo;
import acazia.test.Repository.ProductRepo;
import acazia.test.entity.Category;
import acazia.test.entity.Product;
import acazia.test.model.CategoryModel;
import acazia.test.service.ICategoryService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.io.IOException;
import java.util.List;
import java.util.Optional;

@Service(value = "ICategoryService")
public class CategoryservicceImpl implements ICategoryService {

    @Resource
    CategoryRepo categoryRepository;
    @Resource
    ProductRepo productRepository;

    @Override
    public List<Category> lsCategory() {
        return categoryRepository.findAll();
    }

    @Override
    public Object insert(CategoryModel category) {
        Category categorys = new Category();
        categorys.setName(category.getCategoryName());
        categorys.setTag(category.getTag());
        categorys.setId(category.getId());

        return this.categoryRepository.save(categorys);
    }

    @Override
    public Category update(CategoryModel category) {
        Optional<Category> categoryOtp = categoryRepository.findByTag(category.getTag());
        Category categorys = new Category();
        if (categoryOtp.isPresent()) {
            categorys.setName(category.getCategoryName());
            categorys.setTag(category.getTag());
            categorys.setId(category.getId());
            return this.categoryRepository.save(categorys);
        }
        return this.categoryRepository.save(categorys);
    }

    @Override
    public void delete(String tag) throws IOException {
        Category category = categoryRepository.findByTagAndTag(tag);
        Product product = new Product();
        try {
            if (product.getCategoryTag() == null) {
            }
            categoryRepository.delete(category);
        } catch (Exception e) {
            System.out.println("khong the xoa khi cos rang buoc du lieu ben product" + e);
        }

    }
    @Override
    public Optional<Category> findByTag(String tag) {
        return categoryRepository.findByTag(tag);
    }

}
