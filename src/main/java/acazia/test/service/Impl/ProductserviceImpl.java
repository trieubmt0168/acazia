package acazia.test.service.Impl;

import acazia.test.Repository.ProductRepo;
import acazia.test.entity.Product;
import acazia.test.model.ProductModel;
import acazia.test.service.IProductSevice;
import org.springframework.stereotype.Service;


import javax.annotation.Resource;
import java.util.List;
import java.util.Optional;

@Service(value = "IProductSevice")
public class ProductserviceImpl implements IProductSevice {
    @Resource
    ProductRepo productRepo;

    @Override
    public List<Product> lsCategory() {
        return productRepo.findAll();
    }

    @Override
    public Object insert(ProductModel product) {
        Product products = new Product();
        products.setName(product.getName());
        products.setCategoryTag(product.getCategoryTag());
        products.setPrice(product.getPrice());
        return productRepo.save(products);
    }
    @Override
    public Product update(ProductModel product) {
        Optional<Product> productOtp = productRepo.findById(product.getId());
         Product products = new Product();
        if (productOtp.isPresent()) {
            products.setName(product.getName());
            products.setCategoryTag(product.getCategoryTag());
            products.setPrice(product.getPrice());
        }
        return this.productRepo.save(products);
    }

    @Override
    public void delete(Long id) {
        Product products = productRepo.getOne(id);
         productRepo.delete(products);
    }

    @Override
    public Optional<Product> findById(Long id) {
        return productRepo.findById(id);
    }

    @Override
    public List<Object[]> findByTag(String categoryName) {
//        categoryName = "%" + categoryName + "%";
        List<Object[]>  ls = productRepo.findByName(categoryName);
        System.out.println(ls.size());
        return ls;
    }

}
